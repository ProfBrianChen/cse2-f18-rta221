//Robert Allen
//9/25/18
//CSE002-310
//This program uses switch to put two values together and come up with its slang
import java.util.Scanner;//imports the scanner
import java.util.Random;//imports the random generator 
public class CrapsSwitch{
  public static void main(String args[]){//main method that all java prgrams need
    Random randGen = new Random();//initializes the random generator 
    Scanner myScanner = new Scanner(System.in);//initializes the scanner 
    int die1 = 1;//initializes the first die 
    int die2 = 1;//initializes the secon die 
    String result = ""; //intializes the string to be printed
    System.out.println("If you'd like to randomly cast dice, enter 0. If not, enter 1: ");//asking if youd like to randomly throw the dice or pick the values
    int diceNum = myScanner.nextInt();//gets the preference form the user
    switch(diceNum){//based on what the user puts in, the switch will do either the first or second operation
      case 0:
        die1 = randGen.nextInt(6) +1;//generates two random dice throws
        die2 = randGen.nextInt(6) +1;
        break;
      case 1://gets the two values from the user
        System.out.println("Please enter the value of the first die: ");
        die1 = myScanner.nextInt();
        System.out.println("Please enter the value of the second die: ");
        die2 = myScanner.nextInt();
        break;
      default: System.out.println("Please input a valid number.");
    }
    //the following switches takes the each case from die1 and pairs it with the cases from die2 to get the slang
    switch(die1){
      case 1: switch(die2){
				case 1: result = "Snake Eyes";
					break;
				case 2: result = "Ace Deuce";
					break;
				case 3: result = "Easy Four";
					break;
				case 4: result = "Fever Five";
					break;
				case 5: result = "Easy Six";
					break;
				case 6: result = "Seven Out";
					break;
				default: result = "invalid number";
					break;
			}
				break;
      case 2: switch(die2){
				case 1: result = "Ace Deuce";
					break;
				case 2: result = "Hard Four";
					break;
				case 3: result = "Fever Five";
					break;
				case 4: result = "Easy Six";
					break;
				case 5: result = "Seven Out";
					break;
				case 6: result = "Easy Eight";
					break;
				default: result = "invalid number";
					break;
			}
				break;
      case 3: switch(die2){
				case 1: result = "Easy Four";
					break;
				case 2: result = "Fever Five";
					break;
				case 3: result = "Hard Six";
					break;
				case 4: result = "Seven Out";
					break;
				case 5: result = "Easy Eight";
					break;
				case 6: result = "Nine";
					break;
				default: result = "invalid number";
					break;
			}
				break;
			case 4: switch(die2){
				case 1: result = "Fever Five";
					break;
				case 2: result = "Easy Six";
					break;
				case 3: result = "Seven Out";
					break;
				case 4: result = "Hard Eight";
					break;
				case 5: result = "Nine";
					break;
				case 6: result = "Easy Ten";
					break;
				default: result = "invalid number";
					break;
			}
				break;
      case 5: switch(die2){
				case 1: result = "Easy Six";
					break;
				case 2: result = "Seven Out";
					break;
				case 3: result = "Easy Eight";
					break;
				case 4: result = "Nine";
					break;
				case 5: result = "Hard Ten";
					break;
				case 6: result = "Yo-leven";
					break;
				default: result = "invalid number";
					break;
			}
				break;
      case 6: switch(die2){
				case 1: result = "Seven Out";
					break;
				case 2: result = "Easy Eight";
					break;
				case 3: result = "Nine";
					break;
				case 4: result = "Easy Ten";
					break;
				case 5: result = "Yo-leven";
					break;
				case 6: result = "Boxcars";
					break;
				default: result = "invalid number";
					break;
    	}
				break;
			default: result = "invalid number";
					break;
    }
		System.out.println(result);
}
}