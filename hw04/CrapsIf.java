//Robert Allen
//9/24/18
//CSE002-310
//HW04
import java.util.Random;//imports the random generator
import java.util.Scanner;//imports the scanner
public class CrapsIf{
  public static void main(String args[]){//main method that all java programs need
    Random randGen = new Random();//starts the random generator
    Scanner myScanner = new Scanner(System.in);//starts the scanner
    System.out.print("If you'd like to randomly cast dice, enter 0. If not, enter another number: ");//asking the user what they want to do
    double answer = myScanner.nextDouble();//gets the input from the user
    int die1 = 1;//initializes the first die
    int die2 = 1;//initializes the secind die
    if(answer == 0){//if the user chooses, it generates the random die throws
      die1 = randGen.nextInt(6) + 1;
      die2 = randGen.nextInt(6) + 1;
    }
    else{//this is for the user to input his/her wanted values for the dice
      System.out.print("Please enter the value of the first die:");
      die1 = myScanner.nextInt();
      System.out.print("Please enter the value of the second die:");
      die2 = myScanner.nextInt();
    }
    if(die1 >= 7 || die2 >= 7 || die1 == 0 || die2 == 0){//these are invalid die numbers, so you will see an error messege
        System.out.println("The entered values are invalid.");
      }
    //the following if statements pair the dice and print the according slang
    if(die1 == 1 && die2 == 1){
      System.out.println("Snake eyes");
    }
    if((die1 == 1 && die2 ==2) || (die1 == 2 && die2 == 1)){
      System.out.println("Ace Deuce");
    }
    if((die1 == 1 && die2 ==3) || (die1 == 3 && die2 == 1)){
      System.out.println("Easy Four");
    }
    if((die1 == 1 && die2 ==4) || (die1 == 4 && die2 == 1)){
      System.out.println("Fever Five");
    }
    if((die1 == 1 && die2 ==5) || (die1 == 5 && die2 == 1)){
      System.out.println("Easy Six");
    }
    if((die1 == 1 && die2 ==6) || (die1 == 6 && die2 == 1)){
      System.out.println("Seven Out");
    }
    if(die1 == 2 && die2 ==2){
      System.out.println("Hard Four");
    }
    if((die1 == 3 && die2 ==2) || (die1 == 2 && die2 == 3)){
      System.out.println("Fever Five");
    }
    if((die1 == 4 && die2 ==2) || (die1 == 2 && die2 == 4)){
      System.out.println("Easy Six");
    }
    if((die1 == 5 && die2 ==2) || (die1 == 2 && die2 == 5)){
      System.out.println("Seven Out");
    }
    if((die1 == 6 && die2 ==2) || (die1 == 2 && die2 == 6)){
      System.out.println("Easy Eight");
    }
    if(die1 == 3 && die2 ==3){
      System.out.println("Hard Six");
    }
    if((die1 == 3 && die2 ==4) || (die1 == 4 && die2 == 3)){
      System.out.println("Seven Out");
    }
    if((die1 == 3 && die2 ==5) || (die1 == 5 && die2 == 3)){
      System.out.println("Easy Eight");
    }
    if((die1 == 3 && die2 ==6) || (die1 == 6 && die2 == 3)){
      System.out.println("Nine");
    }
    if(die1 == 4 && die2 ==4){
      System.out.println("Hard Eight");
    }
    if((die1 == 4 && die2 ==5) || (die1 == 5 && die2 == 4)){
      System.out.println("Nine");
    }
    if((die1 == 4 && die2 ==6) || (die1 == 6 && die2 == 4)){
      System.out.println("Easy Ten");
    }
    if(die1 == 5 && die2 ==5){
      System.out.println("Hard Ten");
    }
    if((die1 == 5 && die2 ==6) || (die1 == 6 && die2 == 5)){
      System.out.println("Yo-leven");
    }
    if(die1 == 6 && die2 ==6){
      System.out.println("Boxcars");
    }
  }
}