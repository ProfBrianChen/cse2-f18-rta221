//Robert Allen
//9/10/2018
//CSE 002-310
//Arithmetic
public class Arithmetic{
  
  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    //the tax rate
    double paSalesTax = 0.06;
    
    double priceOfPants = numPants * pantsPrice;//calculating the raw price of the pants
    double priceOfShirts = numShirts * shirtPrice;//calculating the raw price of the shirts
    double priceOfBelts = numBelts * beltCost;//calculating the raw price of the belts
    //to find the sales tax of pants, multiply raw price by sales tax. Then use calulations to convert it into integer
    double pantSalesTx = paSalesTax * priceOfPants;
    double pantSalesTx1 = pantSalesTx * 100;
    double pantSalesTx2 = (int)pantSalesTx1 / 100;
    //to find the sales tax of shirts, multiply raw price by sales tax. Then use calculations to convert from double to integer
    double shirtSalesTx = paSalesTax * priceOfShirts;
    double shirtSalesTx1 = shirtSalesTx * 100;
    double shirtSalesTx2 = (int)shirtSalesTx1 / 100;
    //to find the sales tax of belts, multiply raw price by sales tax. Then use calculations to convert from double to integer
    double beltSalesTx = paSalesTax * priceOfBelts;
    double beltSalesTx1 = beltSalesTx * 100;
    double beltSalesTx2 = (int)beltSalesTx1 / 100;
    double totCostPurchaseNoTx = priceOfBelts + priceOfShirts + priceOfPants;//adding the raw prices together for total w/o tax
    double totSalesTx = pantSalesTx2 + shirtSalesTx2 + beltSalesTx2;//finding the total amount of taxes
    double totCostPurchase = totSalesTx + totCostPurchaseNoTx;//finding the total price paid
    
    
    //print statements for the required material: prices w/o tax, total tax, total price w/o tax, and total cost of all
    System.out.println("The price of pants w/o tax is $" +priceOfPants + " and the sales tax is $" +pantSalesTx2);
    System.out.println("The price of shirts w/o tax is $" +priceOfShirts + " and the sales tax is $" +shirtSalesTx2);
    System.out.println("The price of belts w/o tax is $" +priceOfBelts + " and the sales tax is $" +beltSalesTx2);
    System.out.println("The combined price of the items w/o tax is $" +totCostPurchaseNoTx);
    System.out.println("The total sales tax of all the items is $" +totSalesTx);
    System.out.println("The total cost with sales tax is $" +totCostPurchase);
  }
}