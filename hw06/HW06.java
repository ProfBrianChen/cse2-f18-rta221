//Robert Allen
//CSE002-310
//HW06 asks for how many rows and creates a pattern
import java.util.Scanner;//import the scanner
public class HW06{
  public static void main(String[] args){//main method that every java program needs
    Scanner myScanner = new Scanner(System.in);//initializing the scanner
    System.out.println("How many rows would you like to print?");//asking the user for how many rows they want
    int row = myScanner.nextInt();//gets the variable from the user input
    for(int i = 1;i<=row;i++){
      for(int j = 1;j<=row;j++){
        if(i==j||i+j==row+1)//the two conditions that there will be an empty space
          System.out.print(" ");
        else
          System.out.print("*");
      }
      System.out.println("");
    }
  }
}