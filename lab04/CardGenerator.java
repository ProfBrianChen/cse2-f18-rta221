//Robert Allen
//CSE 002 -310
//9/20/18
//This program is designed to randomly generate a number and assign that number to a card that has a value and a suit.
import java.util.Random;//this imports the thing to create the random numbers
public class CardGenerator{//main method required by every Java program
  public static void main(String args[]){
    Random randGen = new Random();//this starts the random number generator
    int card = randGen.nextInt(53) + 1;//this sets the random number as the variable card. 53 is in the quotes becasue we need it to go to 52, and the plus one is to make sure 0 isnt a value
    //each if statement has a specific value that when it is equal to, it will print an assigned value of a card
    if(card == 1){
      System.out.println("You picked the ace of spades");
    }
    if(card == 2){
      System.out.println("You picked the 2 of spades");
    }
    if(card == 3){
      System.out.println("You picked the 3 of spades");
    }
    
    if(card == 4){
      System.out.println("You picked the 4 of spades");
    }
    if(card == 5){
      System.out.println("You picked the 5 of spades");
    }
    if(card == 6){
      System.out.println("You picked the 6 of spades");
    }
    if(card == 7){
      System.out.println("You picked the 7 of spades");
    }
    if(card == 8){
      System.out.println("You picked the 8 of spades");
    }
    if(card == 9){
      System.out.println("You picked the 9 of spades");
    }
    if(card == 10){
      System.out.println("You picked the 10 of spades");
    }
    if(card == 11){
      System.out.println("You picked the jack of spades");
    }
    if(card == 12){
      System.out.println("You picked the queen of spades");
    }
    if(card == 13){
      System.out.println("You picked the king of spades");
    }
    if(card == 14){
      System.out.println("You picked the ace of clubs");
    }
    if(card == 15){
      System.out.println("You picked the 2 of clubs");
    }
    if(card == 16){
      System.out.println("You picked the 3 of clubs");
    }
    if(card == 17){
      System.out.println("You picked the 4 of clubs");
    }
    if(card == 18){
      System.out.println("You picked the 5 of clubs");
    }
    if(card == 19){
      System.out.println("You picked the 6 of clubs");
    }
    if(card == 20){
      System.out.println("You picked the 7 of clubs");
    }
    if(card == 21){
      System.out.println("You picked the 8 of clubs");
    }
    if(card == 22){
      System.out.println("You picked the 9 of clubs");
    }
    if(card == 23){
      System.out.println("You picked the 10 of clubs");
    }
    if(card == 24){
      System.out.println("You picked the jack of clubs");
    }
    if(card == 25){
      System.out.println("You picked the queen of clubs");
    }
    if(card == 26){
      System.out.println("You picked the king of clubs");
    }
    if(card == 27){
      System.out.println("You picked the ace of diamonds");
    }
    if(card == 28){
      System.out.println("You picked the 2 of diamonds");
    }
    if(card == 29){
      System.out.println("You picked the 3 of diamonds");
    }
    if(card == 30){
      System.out.println("You picked the 4 of diamonds");
    }
    if(card == 31){
      System.out.println("You picked the 5 of diamonds");
    }
    if(card == 32){
      System.out.println("You picked the 6 of diamonds");
    }
    if(card == 33){
      System.out.println("You picked the 7 of diamonds");
    }
    if(card == 34){
      System.out.println("You picked the 8 of diamonds");
    }
    if(card == 35){
      System.out.println("You picked the 9 of diamonds");
    }
    if(card == 36){
      System.out.println("You picked the 10 of diamonds");
    }
    if(card == 37){
      System.out.println("You picked the jack of diamonds");
    }
    if(card == 38){
      System.out.println("You picked the queen of diamonds");
    }
    if(card == 39){
      System.out.println("You picked the king of diamonds");
    }
    if(card == 40){
      System.out.println("You picked the ace of hearts");
    }
    if(card == 41){
      System.out.println("You picked the 2 of hearts");
    }
    if(card == 42){
      System.out.println("You picked the 3 of hearts");
    }
    if(card == 43){
      System.out.println("You picked the 4 of hearts");
    }
    if(card == 44){
      System.out.println("You picked the 5 of hearts");
    }
    if(card == 45){
      System.out.println("You picked the 6 of hearts");
    }
    if(card == 46){
      System.out.println("You picked the 7 of hearts");
    }
    if(card == 47){
      System.out.println("You picked the 8 of hearts");
    }
    if(card == 48){
      System.out.println("You picked the 9 of hearts");
    }
    if(card == 49){
      System.out.println("You picked the 10 of hearts");
    }
    if(card == 50){
      System.out.println("You picked the jack of hearts");
    }
    if(card == 51){
      System.out.println("You picked the queen of hearts");
    }
    if(card == 52){
      System.out.println("You picked the king of hearts");
    }
    
    
  }
}