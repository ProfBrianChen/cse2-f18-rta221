//Robert Allen
//CSE 002-310
//9/20/18
//I wrote this program after the one I submitted for lab because I wanted to make a more elegant solution.
//This program will randomly generate the value of the card as well as randomly generate teh suit
import java.util.Random;//this imports the thing to create the random numbers
public class CardGenerator2{//main method required by every Java program
  public static void main(String args[]){
    Random randGen = new Random();//this starts the random number generator
    int card = randGen.nextInt(13) + 1;//this initializes the variable card and assigns it a random value from 1-13
    String cardString ="";//creating the string variable for if the card is a face card
      switch(card){//this will give variable cardString a string value based on it's value. If the value isn't a case, then the value of the card is the number
        //need a case for each face card
        case 1: cardString = "Ace";
          break;
        case 11: cardString = "Jack";
          break;
        case 12: cardString = "Queen";
          break;
        case 13: cardString = "King";
          break;
        default: card = card;//if the case isn't here, then the card will only have it's number value
      }
    int suit = randGen.nextInt(4) + 1;//this initializes the varibale for the suit and gives it a random number from 1-4
    String suitString ="";//initializing the variable for suits
      switch(suit){//this will give suitString a string value based on its number. We are assiging each number to a suit
        case 1: suitString = "Spades";
          break;
        case 2: suitString = "Clubs";
          break;
        case 3: suitString = "Diamonds";
          break;
        case 4: suitString = "Hearts";
          break;
        default: suit = suit;
      }
    //making an if/else statement so that if the card is a face card, it will print what is on the card instead of just the number. If it does not have a special value, it will just print the number.
    if(card == 1 || card > 10){
      System.out.println("You picked the "+ cardString +" of " + suitString);
    }
    else{
      System.out.println("You picked the "+ card + " of " + suitString);
    }
  }
}