//Robert Allen
//9/17/18
//CSE 002-310
//THis program asks for the side length of a square pyramid and the hight and gives you the volume
import java.util.Scanner;//importing the program Scanner
public class Pyramid{//main method required by every Java program
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in);//starts the scanner
    System.out.print("The square side of the pyramid is: ");//asks for the side length of one of the pyramids
    double sidePyr = myScanner.nextDouble();//stores the number
    System.out.print("The height of the pyramid is: ");//asks for the height of the pyramid
    double heightPyr = myScanner.nextDouble();//stores the number
    double volPyr = (sidePyr * sidePyr * heightPyr)/3;//computing the volume of the pyramid
    System.out.println("The volume of the pyramid is: " +volPyr);//prints out the volume of the pyramid
  }
}