//Robert Allen
//9/17/18
//CSE 002-310
//This program will take in the acreage of are that got rain and take the amount of rain and give you the cubic miles of rain
import java.util.Scanner;//importing the program Scanner
public class Convert{//main method required by every Java program
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in);//establishing the scanner
    System.out.print("Enter the affected area in acres: ");//asking for input of acres
    double acreAffected = myScanner.nextDouble();//takes the number and stores it
    System.out.print("Enter the rainfall in the affected area: ");//asking for the amount of rain
    double rainFall = myScanner.nextDouble();//takes the number and stores it
    //do the arithmatic to convert the acres and inches of rain into cubic miles
    double milesAffected = acreAffected / 640;
    double rainMiles = rainFall / 63360;
    double cubicMiles = milesAffected * rainMiles;
    System.out.println(cubicMiles+ " cubic miles");//prints out the cubic miles
  }
}