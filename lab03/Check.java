//Robert Allen
//CSE 002-310
//9/13/18
//Check Program will take input of a cost of dinner and ask for how many people and how much you want to tip, then divide it up evenly
import java.util.Scanner;//importing the program Scanner
public class Check{//main method required by every Java program
  public static void main(String args[]){
    Scanner myScanner = new Scanner( System.in);//this is initializing the scanner
    System.out.print("Enter the original cost of the check in the form xx.xx: ");//Prints the question asking for the inital cost of the check
    double checkCost = myScanner.nextDouble();//user will input the cost
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): " );
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    System.out.print("Enter the number of people who went out to dinner: ");//asking user to enter the number of people at dinner
    int numPeople = myScanner.nextInt();//user enters the number of people
    double totalCost;//initializes the variable totalCost
    double costPerPerson;//initialize the variable costPerPerson
    int dollars, dimes, pennies;//intiialize the variables dollars, dimes, and pennies
    totalCost = checkCost * (1 + tipPercent);//finds the total cost of the bill with the tip included
    costPerPerson = totalCost / numPeople;//divides the total cost by the number of people at dinner
    dollars = (int)costPerPerson;//finds the whole number of dollars that each person owes
    dimes = (int)(costPerPerson * 10) % 10;//finds the number of dimes each person owes
    pennies = (int)(costPerPerson * 100) % 10;//finds the number of pennies that each person owes
    System.out.println("Each person in the group owes $" + dollars + "." + dimes + pennies); //prints how much each person owes
  }
}
  