//Robert Allen
//9/6/18
//CSE 002 - 310
//print the number of minutes for each trip, print the number of counts for each trip, print the distance of each trip in miles, print the distance for the two trips combined

public class Cyclometer {
    	// main method required for every Java program
   	public static void main(String[] args) {  
    // These are the variables for how many seconds each trip took and the number of rotations for each trip
	  int secsTrip1=480;  // seconds for trip1
    double secsTrip2=3220;  // seconds for trip2
		int countsTrip1=1561;  // rotations for trip1
		int countsTrip2=9037; // rotations for trip2
      
    double wheelDiameter=27.0;  //diameter of the wheel
  	double PI=3.14159; //count for PI
  	int feetPerMile=5280;  //feet in a mile
  	int inchesPerFoot=12;   //inches in a foot
  	double secondsPerMinute=60;  //seconds in a minute
	  double distanceTrip1, distanceTrip2,totalDistance;  //initializing these variables
      
    System.out.println("Trip 1 took "+(secsTrip1/secondsPerMinute)+" minutes and had "+countsTrip1+" counts.");
    //print how many minutes and how many rotations in trip1
	  System.out.println("Trip 2 took "+(secsTrip2/secondsPerMinute)+" minutes and had "+countsTrip2+" counts.");
    //print how many minutes and rotations in trip2
      
    distanceTrip1=countsTrip1*wheelDiameter*PI;
    	// Above gives distance in inches
    	//(for each count, a rotation of the wheel travels
    	//the diameter in inches times PI)
	  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
	  distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;//finds the distance of trip2
	  totalDistance=distanceTrip1+distanceTrip2;//finds the total distances of both trips
      
    //Print out the output data.
    System.out.println("Trip 1 was "+distanceTrip1+" miles.");
      //prints how many miles trip1 was
	  System.out.println("Trip 2 was "+distanceTrip2+" miles.");
      //prints how many miles trip2 was
	  System.out.println("The total distance was "+totalDistance+" miles.");
      //prints the total distance for both trips



	}  //end of main method   
} //end of class