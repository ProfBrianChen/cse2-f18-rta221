//Robert Allen
//CSE 002 Lab 7
//The point of this program is to randomly generate words and tie them together to form a sentance
import java.util.Random;//imports the random generator
import java.util.Scanner;//imports the scanner
public class MethodsPractice{//main method every java program needs
  public static void main(String[] args){
    Scanner myScanner = new Scanner(System.in);//starting the scanner
    int count =1;//initialize the count
    while(count==1){//while statement
      Random randomGenerator = new Random();//starting the random generator
      int randomInt = randomGenerator.nextInt(10);
      String adjective1 = adjective(randomInt);//calling adjectives
      int randomInt1 = randomGenerator.nextInt(10);
      String subject1 = subject(randomInt1);
      int randomInt2 = randomGenerator.nextInt(10);
      String verb1 = verb(randomInt2);
      int randomInt3 = randomGenerator.nextInt(10);
      String object1 = object(randomInt3);
      System.out.println("The "+adjective1+" "+subject1+" "+verb1+" the "+object1);//strings the words together
      System.out.println("Would you like another story? Enter 1 for yes and 0 for no.");//asks the user for input
      int condition = myScanner.nextInt();
      if(condition==0){
        count++;
      }
    }
  }
  public static String adjective(int a){//method for the first word
    String adjective1 = " ";
    switch(a){
      case 0: adjective1 = "smelly";
      break;
      case 1: adjective1 = "spectacular";
      break;
      case 2: adjective1 = "huge";
      break;
      case 3: adjective1 = "slow";
      break;
      case 4: adjective1 = "gangly";
      break;
      case 5: adjective1 = "ugly";
      break;
      case 6: adjective1 = "aggressive";
      break;
      case 7: adjective1 = "eager";
      break;
      case 8: adjective1 = "faithful";
      break;
      case 9: adjective1 = "jealous";
      break;
    }
    return adjective1;
  }
  
  public static String subject(int a){//method for the second word
    String subject1 = " ";
    switch(a){
      case 0: subject1 = "autobot";
      break;
      case 1: subject1 = "decepticon";
      break;
      case 2: subject1 = "pikachu";
      break;
      case 3: subject1 = "robin hood";
      break;
      case 4: subject1 = "matt damon";
      break;
      case 5: subject1 = "george clooney";
      break;
      case 6: subject1 = "chris pratt";
      break;
      case 7: subject1 = "dinasour that attacks chris pratt";
      break;
      case 8: subject1 = "tony stark";
      break;
      case 9: subject1 = "clark kent";
      break;
    }
    return subject1;
  }
  
  public static String verb(int a){//method for the third word
    String verb1 = " ";
    switch(a){
      case 0: verb1 = "flew";
      break;
      case 1: verb1 = "shot";
      break;
      case 2: verb1 = "survived";
      break;
      case 3: verb1 = "tackled";
      break;
      case 4: verb1 = "smooched";
      break;
      case 5: verb1 = "spooked";
      break;
      case 6: verb1 = "seduced";
      break;
      case 7: verb1 = "slapped";
      break;
      case 8: verb1 = "destroyed";
      break;
      case 9: verb1 = "shoulder checked";
      break;
    }
    return verb1;
  }
  public static String object(int a){//method for the fourth word
    String object1 = " ";
    switch(a){
      case 0: object1 = "log";
      break;
      case 1: object1 = "rabbit";
      break;
      case 2: object1 = "television";
      break;
      case 3: object1 = "chair";
      break;
      case 4: object1 = "bus";
      break;
      case 5: object1 = "pumpkin";
      break;
      case 6: object1 = "cake";
      break;
      case 7: object1 = "frat boy";
      break;
      case 8: object1 = "diamond";
      break;
      case 9: object1 = "powerpoint";
      break;
    }
    return object1;
  }
}