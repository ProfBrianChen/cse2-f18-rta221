//Robert Allen
//CSE002-310
//10/11/18
//Lab6_PatternA creates a pattern of outputs that were specified by the user
import java.util.Scanner;//imports the scanner
public class PatternA{
  public static void main(String[] args){//main method that every java program has to have
    Scanner myScanner = new Scanner(System.in);//starting and declaring the scanner
    System.out.println("Enter an integer 1-10 for how many rows you'd like: ");//asking the user for how many rows they/d like
    int numbRows = myScanner.nextInt();//takes the number that the user types in
    while(numbRows<1 || numbRows>10){//this loop will reject any value that is not in between 1-10 and ask for a new value
      System.out.println("Please enter a valid number: ");
      numbRows = myScanner.nextInt();
    }
    for(int i = 1; i<= numbRows; i++){//this loop increments i until it is equal to the user input. It controls the number of rows
      for(int j = 1; j<=i; j++){//this loop strings together the numbers that are in each line of the output
        System.out.print(j + " ");
      }
      System.out.println(" ");
    }
  }
}
