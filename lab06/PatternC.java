//Robert Allen
//CSE002-310
//10/11/18
//Lab6_PatternC creates output from user input and lines it on the right instead of the left
import java.util.Scanner;//imports the scanner
public class PatternC{
  public static void main(String[] args){//main method that every java program has to have
    Scanner myScanner = new Scanner(System.in);//starts and declares the scanner
    System.out.println("Enter an integer 1-10 for how many rows you'd like: ");//asks the user how many rows they want
    int numbRows = myScanner.nextInt();//takes the entered value from the user
    while(numbRows<1 || numbRows>10){//loop decides if the value entered was valid. If not, asks for a new one
      System.out.println("Please enter a valid number: ");
      numbRows = myScanner.nextInt();
    }
    for(int i = 1; i<= numbRows; i++){//dictates the number of rows
      for(int j = numbRows; j>i;j--){//this dictates the number of spaces in each row before the numbers
       System.out.print(" "); 
      }
      for(int j = i; j>=1; j--){//dictates what the numbers are in each row
        System.out.print(j);
      }
      System.out.println(" ");
    }
  }
}
