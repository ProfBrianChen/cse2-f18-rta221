//Robert Allen
//CSE002-310
//10/11/18
//Lab6_PatternB takes user input and creates a certain pattern of output
import java.util.Scanner;//imports the scanner
public class PatternB{
  public static void main(String[] args){//main method that every java program needs
    Scanner myScanner = new Scanner(System.in);//starts and declares the new scanner
    System.out.println("Enter an integer 1-10 for how many rows you'd like: ");//asks the user how many rows they'd like
    int numbRows = myScanner.nextInt();//takes the value from the user
    while(numbRows<1 || numbRows>10){//this loop reads the entered value and asks for a new value if present one is not in scope
      System.out.println("Please enter a valid number: ");
      numbRows = myScanner.nextInt();
    }
    for(int i = numbRows; i>=1; i--){//this loop starts i at the enterd value and decreases it to 1. It handles the number of rows
      for(int j = 1; j<=i; j++){//this loop increases j from 1 to whatever i is to get the output on each row
        System.out.print(j + " ");
      }
      System.out.println(" ");
    }
  }
}
