//Robert Allen
//CSE002-310
//10/11/18
//Lab6_PatternD creates output from user input
import java.util.Scanner;//imports the scanner
public class PatternD{
  public static void main(String[] args){//main method that every java program requires
    Scanner myScanner = new Scanner(System.in);//starts and declares the scanner
    System.out.println("Enter an integer 1-10 for how many rows you'd like: ");//asks the user for how many rows they'd like
    int numbRows = myScanner.nextInt();//takes the entered value from the user
    while(numbRows<1 || numbRows>10){//loop determines if the entered value is valid. If not, asks for a new one
      System.out.println("Please enter a valid number: ");
      numbRows = myScanner.nextInt();
    }
    for(int i = 1; i<=numbRows; i++){//dictates the number of rows
      for(int j = (numbRows+1)-i; j>=1; j--){//dictates the ouput for each row
        System.out.print(j + " ");
      }
      System.out.println(" ");
    }
  }
}
