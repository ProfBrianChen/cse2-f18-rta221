//Robert Allen
//9/3/2018
//CSE 002-310
//WelcomeClass
public class WelcomeClass{
  
  public static void main(String args[]){
    //prints WelcomeClass out
    System.out.println("      ------------");
    System.out.println("    | W E L C O M E |");
    System.out.println("      ------------");
    System.out.println("  ^   ^   ^   ^   ^   ^");
    System.out.println(" / \\ / \\ / \\ / \\ / \\ / \\");
    System.out.println("<-R   T   A   2   2   1->");
    System.out.println(" \\ / \\ / \\ / \\ / \\ / \\ /");
    System.out.println("  v   v   v   v   v   v");
  }
}