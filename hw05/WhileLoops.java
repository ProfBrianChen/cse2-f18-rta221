//Robert Allen
//CSE002-310
//HW05
//this program asks the user for how many hands they'd like to generate. Then it determines the probability of getting a four of a kind, three of a kind, a two pair, or a pair.
import java.util.Scanner;//imports the scanner
import java.util.Random;//imports the random generator
public class WhileLoops{
  public static void main(String[] args){//the main method that every java program requires
		Random randGen = new Random();//starts the random generator
    Scanner myScanner = new Scanner(System.in);//starts the scanner
		int[] cards = new int[5];//initializes the arrays to set up the 5 cards picked
		int[] probs = new int[4];//initializes the array to set up the 4 outputs for printing 
		float[] probPrint = new float[4];//initializes the 4 values for print statement
		int[] occurences = new int[13];//counts how many occurences of the card value there are
		int loops;
		int card;
			
		//Asks for loops
		do{
			System.out.println("Enter the amount of hands you wish to generate: ");//asks user how many hands they want 
		  loops = myScanner.nextInt();//gets the users input
    }
    while(loops < 0);//loop will the number of times that the user entered
		
		//do for x loops
		for(int x = 0; x<loops; x++){//run for all hands
		//generates cards
		for(int i = 0; i< 5;i++){//checks each card in a hand
			do{
				card = randGen.nextInt(52)+1;//assigns a value for a card
			}while(card == cards[0] || card == cards[1] || card == cards[2] || card == cards[3] || card == cards[4]);//while all the cards equal each other
			cards[i] = card;	
			occurences[card%13] ++;//show occurence for each value of card by using remander of 13
		}
		boolean presentPair = false;// initalizing to show how a present pair is not true
		
		for(int i = 0;i<13;i++){
			if(occurences[i]==4){//counts how many four ok a kinds
				probs[0]++;//and to the value
			}
			if(occurences[i]==3){//counts how many 3 of a kinds
				probs[1]++;
			}
			if(occurences[i]==2&&presentPair){//see if there is pair with a pair already in the hand
				probs[2]++;
				probs[3]--;
			}
			if(occurences[i]==2){//see if there is a pair without another pair in the hand
				probs[3]++;
				presentPair = true;
			}
			occurences[i] = 0;//sets occurences to 0
		}	
		}
		
		for(int i = 0;i<4;i++){

			probPrint[i] = (float)probs[i]/loops;//gives probability of each type of hand
		}

		System.out.println("The number of loops: "+loops);//print statements for probability
		System.out.format("The probability of Four-of-a-kind: %1.3f\n", probPrint[0]);
		System.out.printf("The probability of Three-of-a-kind: %1.3f\n", probPrint[1]);
		System.out.printf("The probability of Two-pair %1.3f\n", probPrint[2]);
		System.out.printf("The probability of One-pair: %1.3f\n", probPrint[3]);
	}
}